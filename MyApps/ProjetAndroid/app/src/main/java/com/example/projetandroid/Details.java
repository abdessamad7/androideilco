package com.example.projetandroid;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;


public class Details extends AppCompatActivity {

    private String IMAGE_BASE_URL = "https://image.tmdb.org/t/p/w780";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        setTitle("Détails");

        ImageView image = findViewById(R.id.imageView3);
        TextView title = findViewById(R.id.title);
        TextView overView = findViewById(R.id.overview);
        TextView date = findViewById(R.id.date);
        TextView vote = findViewById(R.id.vote);
        TextView voteCount = findViewById(R.id.voteCount);

        Intent intent = getIntent();
        if(intent.hasExtra("title")){
            Glide.with(getApplicationContext())
                    .load(IMAGE_BASE_URL+intent.getStringExtra("poster_path"))
                    .apply(RequestOptions.placeholderOf(R.color.colorPrimary))
                    .into(image);

            String title2 = getIntent().getExtras().getString("title");
            Double vote2 = getIntent().getExtras().getDouble("vote");
            String date2 = getIntent().getExtras().getString("date");
            String overView2 = getIntent().getExtras().getString("overview");
            Integer voteCount2 = getIntent().getExtras().getInt("voteCount");

            title.setText(title2);
            overView.setText(overView2);
            vote.setText(vote2.toString());
            date.setText(date2);
            voteCount.setText(voteCount2.toString());



        }else{
            Toast.makeText(this,"No API data",Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        Intent intent2 = new Intent(this,Info.class);
        startActivity(intent2);
        return super.onOptionsItemSelected(item);
    }
}
