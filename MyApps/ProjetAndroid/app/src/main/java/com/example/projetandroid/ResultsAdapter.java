package com.example.projetandroid;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ResultsAdapter extends RecyclerView.Adapter<ResultsAdapter.ResultsViewHolder> {

    private String IMAGE_BASE_URL = "https://image.tmdb.org/t/p/w780";

    public List<Result> myResults;
    public Context context;

    public ResultsAdapter(List<Result> myResults,Context context) {
        this.myResults = myResults;
        this.context=context;
    }

    @NonNull
    @Override
    public ResultsAdapter.ResultsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.result_item,parent,false);
        return new ResultsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ResultsViewHolder holder, int position) {
        holder.display(myResults.get(position));
    }

    @Override
    public int getItemCount() {
        return myResults.size();
    }

    public class ResultsViewHolder extends RecyclerView.ViewHolder{

        private String IMAGE_BASE_URL = "https://image.tmdb.org/t/p/w780";

        public TextView title;
        public TextView vote;
        public TextView date;
        public ImageView image;

        public ResultsViewHolder(@NonNull View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.title);
            vote = itemView.findViewById(R.id.vote);
            date = itemView.findViewById(R.id.date);
            image = itemView.findViewById(R.id.imageView);
            final Context context = itemView.getContext();

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    if(position != RecyclerView.NO_POSITION){
                        Result clickedDataItem = myResults.get(position);
                        Intent toDetails = new Intent(context,Details.class);
                        toDetails.putExtra("title",myResults.get(position).getOriginalTitle());
                        toDetails.putExtra("poster_path",myResults.get(position).getPosterPath());
                        toDetails.putExtra("vote",myResults.get(position).getVoteAverage());
                        toDetails.putExtra("date",myResults.get(position).getReleaseDate());
                        toDetails.putExtra("overview",myResults.get(position).getOverview());
                        toDetails.putExtra("voteCount",myResults.get(position).getVoteCount());
                        context.startActivity(toDetails);
                        Toast.makeText(view.getContext(),clickedDataItem.getOriginalTitle(),Toast.LENGTH_SHORT).show();
                    }

                }
            });
        }

        void display(Result result){
            title.setText(result.getTitle());
            vote.setText(result.getVoteAverage().toString());
            date.setText(result.getReleaseDate().substring(0,4));
            Glide.with(itemView)
                    .load(IMAGE_BASE_URL + result.getPosterPath())
                    .apply(RequestOptions.placeholderOf(R.color.colorPrimary))
                    .into(image);

        }
    }
}
