package com.example.myapptd2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class DetailsActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        Button ok = findViewById(R.id.ok);
        TextView us2 = findViewById(R.id.user2);
        NewsListApplication app = (NewsListApplication) getApplicationContext();
        us2.setText(app.getLogin());
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent1 = new Intent(DetailsActivity.this, NewsActivity.class);
                startActivity(intent1);
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent1 = new Intent(DetailsActivity.this, NewsActivity.class);
        startActivity(intent1);
    }
}
