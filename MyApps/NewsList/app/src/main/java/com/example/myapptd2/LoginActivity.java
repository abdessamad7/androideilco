package com.example.myapptd2;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class LoginActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        Button loginn =findViewById(R.id.login);

        final EditText editText =findViewById(R.id.editText);


        loginn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent1 = new Intent(LoginActivity.this, NewsActivity.class);
                intent1.putExtra("login",editText.getText().toString());
                NewsListApplication app = (NewsListApplication) getApplicationContext();
                app.setLogin(editText.getText().toString());
                startActivity(intent1);


            }
        });




    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        Intent startMain = new Intent(Intent.ACTION_MAIN);
        startMain.addCategory(Intent.CATEGORY_HOME);
        startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(startMain);
    }
    private static final String TAG ="MyAppTD2";
    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(TAG,"terminaison de lactivity "+getLocalClassName());
    }


}
