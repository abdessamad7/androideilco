package com.example.myapptd2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class NewsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);

        Button det =findViewById(R.id.det);
        Button logout=findViewById(R.id.logout);
        Button about =findViewById(R.id.about);
        TextView user=findViewById(R.id.user);

        det.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent2 = new Intent(NewsActivity.this, DetailsActivity.class);
                startActivity(intent2);
            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent3 = new Intent(NewsActivity.this, LoginActivity.class);
                startActivity(intent3);

            }
        });

        about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = "http://android.busin.fr/";
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(intent);
            }
        });

        Intent intent4 = getIntent();
        String us = intent4.getStringExtra("login");
        user.setText(us);
    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        Intent startMain = new Intent(Intent.ACTION_MAIN);
        startMain.addCategory(Intent.CATEGORY_HOME);
        startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(startMain);

    }
    private static final String TAG ="MyAppTD2";
    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(TAG,"terminaison de lactivity "+getLocalClassName());
    }



}
