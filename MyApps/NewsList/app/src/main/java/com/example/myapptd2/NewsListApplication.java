package com.example.myapptd2;

public class NewsListApplication extends android.app.Application {

    private String login;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        this.login=null;
    }
}
