package com.example.adaoudi.tdapp1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private EditText etGauche;
    private EditText etDroit;
    private TextView tvResult;
    private Button egal, rc, add, sub, mult, div;
    private Button un,deux,trois,quatre,cinq,six,sept,huit,neuf;
    private float res;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        etGauche =(EditText) findViewById(R.id.editText);
        etDroit =(EditText) findViewById(R.id.editText2);
        tvResult =(TextView)findViewById(R.id.textView2);

        Button egal=(Button)findViewById(R.id.button);
        Button add=(Button)findViewById(R.id.button1);
        Button sub=(Button)findViewById(R.id.button2);
        Button mult=(Button)findViewById(R.id.button3);
        Button div=(Button)findViewById(R.id.button4);
        Button rc=(Button)findViewById(R.id.buttonrc);

        Button un=(Button)findViewById(R.id.button11);
        Button deux=(Button)findViewById(R.id.button22);
        Button trois=(Button)findViewById(R.id.button33);
        Button quatre=(Button)findViewById(R.id.button44);
        Button cinq=(Button)findViewById(R.id.button55);
        Button six=(Button)findViewById(R.id.button66);
        Button sept=(Button)findViewById(R.id.button77);
        Button huit=(Button)findViewById(R.id.button88);
        Button neuf=(Button)findViewById(R.id.button99);




        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                res = Integer.parseInt(etGauche.getText().toString())+ Integer.parseInt(etDroit.getText().toString());
            }
        });

        sub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                res = Integer.parseInt(etGauche.getText().toString())- Integer.parseInt(etDroit.getText().toString());
            }
        });

        mult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                res = Integer.parseInt(etGauche.getText().toString())* Integer.parseInt(etDroit.getText().toString());
            }
        });

        div.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                res = Integer.parseInt(etGauche.getText().toString())/Integer.parseInt(etDroit.getText().toString());
            }
        });

        rc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etDroit.setText(null);
                etGauche.setText(null);
            }
        });
        egal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvResult.setText(String.valueOf(res));
            }
        });



    }
}
