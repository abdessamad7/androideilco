package com.example.adaoudi.td3list;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

public class MyVideoGamesAdapter  extends RecyclerView.Adapter<MyVideoGamesViewHolder>{
    private List<JeuVideo> mesJeux;
    public MyVideoGamesAdapter(List<JeuVideo> mesJeux) {
        this.mesJeux = mesJeux;
    }


    @NonNull
    @Override
    public MyVideoGamesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_jeu_video,
                parent, false);
        return new MyVideoGamesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyVideoGamesViewHolder holder, int position) {
        holder.display(mesJeux.get(position));
    }

    @Override
    public int getItemCount() {
        return mesJeux.size();
    }
}
