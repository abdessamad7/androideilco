package com.example.adaoudi.td3list;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView myRecyclerView = findViewById(R.id.myRecyclerView);
        myRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        ArrayList<JeuVideo> mesJeux = new ArrayList<>();
        JeuVideo game1 = new JeuVideo("game 1",10.00);
        mesJeux.add(game1);
        mesJeux.add(new JeuVideo("game 2",20.00));
        mesJeux.add(new JeuVideo("game 3",30.00));
        mesJeux.add(new JeuVideo("game 4",40.00));
        mesJeux.add(new JeuVideo("game 5",50.00));
        mesJeux.add(new JeuVideo("game 2",20.00));
        mesJeux.add(new JeuVideo("game 3",30.00));
        mesJeux.add(new JeuVideo("game 4",40.00));
        mesJeux.add(new JeuVideo("game 5",50.00));
        mesJeux.add(new JeuVideo("game 2",20.00));
        mesJeux.add(new JeuVideo("game 3",30.00));
        mesJeux.add(new JeuVideo("game 4",40.00));
        mesJeux.add(new JeuVideo("game 5",50.00));
        mesJeux.add(new JeuVideo("game 2",20.00));
        mesJeux.add(new JeuVideo("game 3",30.00));
        mesJeux.add(new JeuVideo("game 4",40.00));
        mesJeux.add(new JeuVideo("game 5",50.00));
        mesJeux.add(new JeuVideo("game 2",20.00));
        mesJeux.add(new JeuVideo("game 3",30.00));
        mesJeux.add(new JeuVideo("game 4",40.00));
        mesJeux.add(new JeuVideo("game 5",50.00));
        myRecyclerView.setAdapter( new MyVideoGamesAdapter(mesJeux));
    }
}
