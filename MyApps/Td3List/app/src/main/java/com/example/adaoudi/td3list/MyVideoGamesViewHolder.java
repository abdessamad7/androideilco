package com.example.adaoudi.td3list;


import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

public class MyVideoGamesViewHolder extends RecyclerView.ViewHolder {
    private TextView name;
    private TextView price;
    public MyVideoGamesViewHolder(@NonNull View itemView) {
        super(itemView);
        name=itemView.findViewById(R.id.name);
        price=itemView.findViewById(R.id.price);

    }
    void display(JeuVideo jeuVideo){
        name.setText(jeuVideo.getName());
        price.setText(jeuVideo.getPrice() + "$");
    }
}
